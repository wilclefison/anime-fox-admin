import Dashboard from "views/Dashboard.jsx";
import Calendar from "views/Calendar.jsx";
//Anime
import Animes from "views/Anime/anime"
import EditarAnimes from "views/Anime/editar"
import CadastrarAnimes from "views/Anime/cadastrar"
//Blog 
import Noticias from "views/Noticias/noticias"
import CadastrarNoticias from "views/Noticias/cadastro"
import EditarNoticias from "views/Noticias/editar"
//Login
import LoginPage from "views/Pages/LoginPage";


var routes = [
  {
    path: "/dashboard",
    layout: "/admin",
    name: "Painel",
    icon: "pe-7s-graph",
    component: Dashboard
  }, {
    collapse: true,
    path: "/Anime",
    name: "Anime",
    state: "openAnimes",
    icon: "pe-7s-plugin",
    views: [
      {
        path: "/animes",
        layout: "/admin",
        name: "Animes",
        mini: "A",
        component: Animes
      }, {
        path: "/editar-anime/:id",
        layout: "/admin",
        name: "Editar Anime",
        mini: "EA",
        component: EditarAnimes,
        invisible: true
      },
      {
        path: "/cadastrar-anime",
        layout: "/admin",
        name: "Cadastrar Anime",
        mini: "CA",
        component: CadastrarAnimes,
      },

    ]
  },
  {
    path: "/noticias",
    layout: "/admin",
    name: "Noticias",
    icon: "pe-7s-news-paper",
    component: Noticias
  },
  {
    path: "/cadastrar-noticias",
    layout: "/admin",
    name: "Cadastrar Noticias",
    icon: "pe-7s-news-paper",
    component: CadastrarNoticias,
    invisible: true
  },
  {
    path: "/editar-noticia/:id",
    layout: "/admin",
    component: EditarNoticias,
    invisible: true
  },
  {
    path: "/calendar",
    layout: "/admin",
    name: "Calendario",
    icon: "pe-7s-date",
    component: Calendar
  },
  {
    path: "/login",
    layout: "/auth",
    name: "Login Page",
    component: LoginPage,
    invisible: true
  }
];
export default routes;
