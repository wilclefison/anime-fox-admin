const statusTv = ['Returning Series', 'Planned', 'In Production', 'Ended', 'Canceled', 'Pilot', 'Finalizada', 'Cancelada', 'Renovada', 'Em Produção']
const categories = [
    { categoria_id: 1, categoria_nome: "Ação" },
    { categoria_id: 2, categoria_nome: "Artes Marciais" },
    { categoria_id: 3, categoria_nome: "Aventura" },
    { categoria_id: 4, categoria_nome: "Comedia" },
    { categoria_id: 5, categoria_nome: "Drama" },
    { categoria_id: 6, categoria_nome: "Ecchi" },
    { categoria_id: 7, categoria_nome: "Escolar" },
    { categoria_id: 8, categoria_nome: "Esporte" },
    { categoria_id: 9, categoria_nome: "Fantasia" },
    { categoria_id: 10, categoria_nome: "Harem" },
    { categoria_id: 11, categoria_nome: "Ficção Científica" },
    { categoria_id: 12, categoria_nome: "Mecha" },
    { categoria_id: 13, categoria_nome: "Mistério" },
    { categoria_id: 14, categoria_nome: "Psicológico" },
    { categoria_id: 15, categoria_nome: "Romance" },
    { categoria_id: 16, categoria_nome: "Seinen" },
    { categoria_id: 17, categoria_nome: "Shounen" },
    { categoria_id: 18, categoria_nome: "Slice of Life" },
    { categoria_id: 19, categoria_nome: "Sobrenatural" },
    { categoria_id: 20, categoria_nome: "Superpoderes" },
    { categoria_id: 21, categoria_nome: "Terror" },
    { categoria_id: 22, categoria_nome: "Musical" }
]

module.exports = {
    statusTv,
    categories
  };
  