
import React, { Component } from "react";
import { Grid, Row, Col, FormGroup } from "react-bootstrap";
import Card from "components/Card/Card.jsx";
import { useForm } from 'react-hook-form'
import SweetAlert from "react-bootstrap-sweetalert";
import { Editor } from '@tinymce/tinymce-react';
import api from "../../services/api";



const removerAcentos = (str) => {
    var string = str.replace(/^\s+|\s+$/g, "");
    var mapaAcentosHex = {
        a: /[\xE0-\xE6]/g,
        A: /[\xC0-\xC6]/g,
        e: /[\xE8-\xEB]/g,
        E: /[\xC8-\xCB]/g,
        i: /[\xEC-\xEF]/g,
        I: /[\xCC-\xCF]/g,
        o: /[\xF2-\xF6]/g,
        O: /[\xD2-\xD6]/g,
        u: /[\xF9-\xFC]/g,
        U: /[\xD9-\xDC]/g,
        c: /\xE7/g,
        C: /\xC7/g,
        n: /\xF1/g,
        N: /\xD1/g
    };

    for (var letra in mapaAcentosHex) {
        var expressaoRegular = mapaAcentosHex[letra];
        string = string.replace(expressaoRegular, letra);
    }
    string = string.replace(/\s+/g, " ")
    string = string.replace(/[\\"”“]/g,"")
    string = string.split(' ').join('-').toLowerCase();

    return string;
}

const FormCad = (props) => {
    const { register, formState, handleSubmit, watch, errors, setValue, triggerValidation } = useForm({
        mode: "onChange",
        reValidateMode: 'onChange'
    })
    const handleEditorChange = (content, editor) => {
        setValue([{ conteudo: content }])
        triggerValidation()
    }
    const uploadImage = async (event) => {
        var images = event.target.files[0];
        const r = new XMLHttpRequest()
        const d = new FormData()

        d.append('image', images)

        r.open('POST', 'https://api.imgur.com/3/image/')
        r.setRequestHeader('Authorization', `Client-ID 2fa62a8d3d4dd28`)
        r.onreadystatechange = function () {
            if (r.status === 200 && r.readyState === 4) {
                let res = JSON.parse(r.responseText)
                setValue([{ img: `https://i.imgur.com/${res.data.id}.png` }])
                triggerValidation()
            }
        }
        r.send(d)
    }
    const generateLink = (event) => {
        var texto = removerAcentos(event.target.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>ª]/gi, ''))
        setValue([{ link: texto }])
        triggerValidation()
    }
    const onSubmit = data => {
        console.log(data)
        api.post(`/admin/cad_noticia`, data).then((response) => {
            if (response.status === 200) {
                props.successEdit('Concluído', 'Anime Cadastrador...')
                setTimeout(function () { window.location = '#/admin/noticias'; }, 2000);
            } else {

            }
        }).catch((erro) => {
            // handle error
            console.log(erro);
        })
    }

    return (
        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
            <FormGroup>
                <label className="col-sm-2 control-label">Titulo</label>
                <div className="col-sm-10">
                    <input
                        name="titulo"
                        component="input"
                        type="text"
                        placeholder="Ex: Novo anime é lançado"
                        className="form-control"
                        onChange={generateLink}
                        ref={register({ required: true })}
                    />
                    {errors.titulo && 'Titulo é um campo obrigatório'}
                </div>

            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Imagem</label>
                <div className="col-sm-10">
                    <input type="file" className="form-control" name="file" onChange={uploadImage} />
                    <input
                        name="img"
                        component="input"
                        type="hidden"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.img && 'Imagem é um campo obrigatório'}
                    <br></br>
                    <div className="img-container">
                        <img alt="..." width="350px" src={`${watch("img")}`} />
                    </div>
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Sinopse</label>
                <div className="col-sm-10">
                    <Editor
                        apiKey='ifpqzun3drrbym5qyp82twa43emvq4vuayj0q3e0zlx70012'
                        initialValue=""
                        init={{
                            height: 500,
                            menubar: false,
                            plugins: [
                                'print code preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons'
                            ],
                            toolbar: 'undo redo code | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment'

                        }}
                        onEditorChange={handleEditorChange}
                    />
                    <input
                        name="conteudo"
                        component="input"
                        type="hidden"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.conteudo && 'Sinopse é um campo obrigatório'}
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Fonte</label>
                <div className="col-sm-10">
                    <input
                        name="fonte"
                        component="input"
                        type="text"
                        placeholder="Ex: ANMTV"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.titulo && 'Titulo é um campo obrigatório'}
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Link</label>
                <div className="col-sm-10">
                    <input
                        name="link"
                        component="input"
                        type="text"
                        placeholder="Ex: nome-do-artigo"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.titulo && 'Link é um campo obrigatório'}
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Destaque</label>
                <div className="col-sm-10">
                    <div className="checkbox">
                        <input
                            name="destaque"
                            id="destaque"
                            component="input"
                            type="checkbox"
                            ref={register}
                        />
                        <label htmlFor="destaque">Ativo</label>
                    </div>
                </div>
            </FormGroup>
            <button type="submit" className="btn-fill btn btn-info" disabled={!formState.isValid}>Submit</button>
        </form>
    )
}

class CadastrarNoticias extends Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: null,
            show: false,
            dados: {}
        };
        this.successEdit = this.successEdit.bind(this);
    }

    successEdit(titulo, texto) {
        this.setState({
            alert: (
                <SweetAlert
                    success
                    style={{ display: "block", marginTop: "-100px" }}
                    title={titulo}
                    onConfirm={() => this.hideAlert()}
                    onCancel={() => this.hideAlert()}
                    confirmBtnBsStyle="info"
                > {texto}
                </SweetAlert>
            )
        });
    }
    htmlAlert(title, HTML) {
        this.setState({
            alert: (
                <SweetAlert
                    style={{ display: "block", marginTop: "-100px" }}
                    title={title}
                    onConfirm={() => this.hideAlert()}
                    onCancel={() => this.hideAlert()}
                    confirmBtnBsStyle="info"
                >{HTML}
                </SweetAlert>
            )
        });
    }
    hideAlert() {
        this.setState({
            alert: null
        });
    }
    componentDidMount() {

    }
    render() {
        return (
            <div className="main-content">
                {this.state.alert}
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <h4 className="title">Cadastrar Animes</h4>
                            <Card
                                content={
                                    <FormCad successEdit={this.successEdit} htmlAlert={this.htmlAlert} />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default CadastrarNoticias;
