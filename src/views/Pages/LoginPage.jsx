import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
  Alert
} from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import api from "../../services/api";
import { login } from "../../services/auth";

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardHidden: true,
      email: "",
      password: "",
      error: ""
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }

  handleSignIn = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("/api/auth", { email, password });
        login(response.data.token);
        this.props.history.push("/admin/dashboard");
      } catch (err) {
        this.setState({
          error:
            "Houve um problema com o login, verifique suas credenciais. T.T"
        });
      }
    }
  };

  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form >
              <Card
                hidden={this.state.cardHidden}
                textCenter
                title="Login"
                content={
                  <div>
                    <FormGroup>
                      <ControlLabel>User</ControlLabel>
                      <FormControl placeholder="XPT@" type="email" onChange={e => this.setState({ email: e.target.value })} />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Password</ControlLabel>
                      <FormControl placeholder="Password" type="password" autoComplete="off" onChange={e => this.setState({ password: e.target.value })} />
                    </FormGroup>
                    {this.state.error && <Alert bsStyle='danger'><strong>{this.state.error}</strong></Alert>}
                  </div>
                }
                legend={
                  <Button bsStyle="info" fill wd onClick={this.handleSignIn}>
                    Login
                  </Button>
                }
                ftTextCenter
              />

            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default LoginPage;
