
import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, OverlayTrigger, Tooltip } from "react-bootstrap";
import Button from "components/CustomButton/CustomButton.jsx";
import { NavLink } from "react-router-dom";
import SweetAlert from "react-bootstrap-sweetalert";
import api from "../../services/api";
import Card from "components/Card/Card.jsx";


const viewPost = <Tooltip id="view">View Post</Tooltip>;
const editPost = <Tooltip id="edit">Edit Post</Tooltip>;
const removePost = <Tooltip id="remove">Remove Post</Tooltip>;

class Animes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: false,
            pages: 0
        };
    }
    deleteAnime = (id, part) => {
        api.delete(`/admin/delanime/${id}`).then((response) => {
            if (response.status === 200) {
                let data = this.state.data;
                data.splice(part.index, 1)
                this.setState({ data })
                this.successDelete()
            } else {

            }
        }).catch((erro) => {
            // handle error
            console.log(erro);
        })
    }
    successDelete() {
        this.setState({
            alert: (
                <SweetAlert
                    success
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Deletado!"
                    onConfirm={() => this.hideAlert()}
                    onCancel={() => this.hideAlert()}
                    confirmBtnBsStyle="info"
                >
                    Anime Deletado
                </SweetAlert>
            )
        });
    }
    confirmDelete(id, part) {
        this.setState({
            alert: (
                <SweetAlert
                    warning
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Tem certeza?"
                    onConfirm={() => this.deleteAnime(id, part)}
                    onCancel={() => this.hideAlert()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Sim, deletar!"
                    cancelBtnText="Cancelar"
                    showCancel
                >
                    Você não vai poder recurar esse anime e seus episodios.
                </SweetAlert>
            )
        });
    }
    getTestData(page, pageSize, sorted, filtered, handleRetrievedData) {
        let url = '/admin/anime';
        let postObject = {
            page: page,
            pageSize: pageSize,
            sorted: sorted,
            filtered: filtered,
        };

        return this.getAnimes(url, postObject).then(response => {
            response.data.rows.forEach((part, index, theArray) => {
                part.actions = (
                    <div style={{ width: '60px', minWidth: 'auto', margin: 'auto' }}>
                        <OverlayTrigger placement="left" overlay={viewPost}>
                            <Button simple icon bsStyle="info">
                                <i className="fa fa-image" />
                            </Button>
                        </OverlayTrigger>
                        <NavLink
                            to={`/admin/editar-anime/${part.anime_id}`}
                        >
                            <OverlayTrigger placement="left" overlay={editPost}>
                                <Button simple icon bsStyle="success">
                                    <i className="fa fa-edit" />
                                </Button>
                            </OverlayTrigger>
                        </NavLink>
                        <OverlayTrigger placement="left" overlay={removePost}>
                            <Button simple icon bsStyle="danger" onClick={() => {
                                this.confirmDelete(part.anime_id, part)
                            }}>
                                <i className="fa fa-times" />
                            </Button>
                        </OverlayTrigger>
                    </div>
                )
                theArray[index] = part;
            });

            handleRetrievedData(response)
        }).catch(response => console.log(response));
    }
    getAnimes(url, params = {}) {
        return api.post(url, params)
    }
    successEdit(titulo, texto) {
        this.setState({
            alert: (
                <SweetAlert
                    success
                    style={{ display: "block", marginTop: "-100px" }}
                    title={titulo}
                    onConfirm={() => this.hideAlert()}
                    onCancel={() => this.hideAlert()}
                    confirmBtnBsStyle="info"
                > {texto}
                </SweetAlert>
            )
        });
    }
    hideAlert() {
        this.setState({
            alert: null
        });
    }
    render() {
        const { data } = this.state;

        return (
            <div className="main-content">
                {this.state.alert}
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <h4 className="title">Lista de animes cadastrados</h4>
                            <Card
                                content={
                                    <ReactTable
                                        data={data}
                                        pages={this.state.pages}
                                        columns={[
                                            {
                                                Header: "Thumb",
                                                id: "img_thumb",
                                                accessor: e => (
                                                    <div className="img-container">
                                                        <img alt="..." src={`https://image.tmdb.org/t/p/w92${e.anime_imagem}`} />
                                                    </div>
                                                ),
                                                sortable: false,
                                                filterable: false
                                            },
                                            {
                                                Header: "Id",
                                                accessor: "anime_id",
                                                filterable: false
                                            },
                                            {
                                                Header: "Nome",
                                                accessor: "anime_nome"
                                            },
                                            {
                                                Header: "Descrição",
                                                accessor: "anime_sinopse",
                                                sortable: false,
                                                filterable: false
                                            }, {
                                                Header: "",
                                                accessor: "actions",
                                                sortable: false,
                                                filterable: false
                                            }
                                        ]}
                                        defaultPageSize={10}
                                        className="-striped -highlight"
                                        loading={this.state.loading}
                                        showPagination={true}
                                        filterable
                                        showPaginationTop={false}
                                        showPaginationBottom={true}
                                        pageSizeOptions={[5, 10, 20, 25, 50, 100]}
                                        manual // this would indicate that server side pagination has been enabled 
                                        onFetchData={(state, instance) => {
                                            this.setState({ loading: true });
                                            this.getTestData(state.page, state.pageSize, state.sorted, state.filtered, (res) => {
                                                this.setState({
                                                    data: res.data.rows,
                                                    pages: res.data.pages,
                                                    loading: false
                                                })
                                            });
                                        }}
                                    />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Animes;
