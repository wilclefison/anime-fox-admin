
import React, { Component } from "react";
import { Grid, Row, Col, FormGroup } from "react-bootstrap";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { useForm } from 'react-hook-form'
import SweetAlert from "react-bootstrap-sweetalert";
import { statusTv, categories } from '../../variables/animes'
import api from "../../services/api";

const removerAcentos = (str) => {
    var string = str.replace(/^\s+|\s+$/g, "");
    var mapaAcentosHex = {
        a: /[\xE0-\xE6]/g,
        A: /[\xC0-\xC6]/g,
        e: /[\xE8-\xEB]/g,
        E: /[\xC8-\xCB]/g,
        i: /[\xEC-\xEF]/g,
        I: /[\xCC-\xCF]/g,
        o: /[\xF2-\xF6]/g,
        O: /[\xD2-\xD6]/g,
        u: /[\xF9-\xFC]/g,
        U: /[\xD9-\xDC]/g,
        c: /\xE7/g,
        C: /\xC7/g,
        n: /\xF1/g,
        N: /\xD1/g
    };

    for (var letra in mapaAcentosHex) {
        var expressaoRegular = mapaAcentosHex[letra];
        string = string.replace(expressaoRegular, letra);
    }
    string = string.replace(/\s+/g, " ")
    string = string.replace(/[\\"”“]/g,"")
    string = string.split(' ').join('-').toLowerCase();

    return string;
}
const FormEdit = (props) => {
    const { register, formState, handleSubmit, watch, errors, setValue, triggerValidation } = useForm({
        mode: "onChange",
        reValidateMode: 'onChange', defaultValues: props.data
    })
    const LoadData = TMDBID => {
        //alert(TMDBID)
    }
    const generateLink = (event) => {
        var texto = removerAcentos(event.target.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>ª]/gi, ''))
        setValue([{ url: texto }])
        triggerValidation()
    }
    const onSubmit = data => {
        api.post(`/admin/anime/${data.anime_id}`, data).then((response) => {
            if (response.status === 200) {
                props.successEdit('Concluído', 'Edição feita com sucesso')
            } else {

            }
        }).catch((erro) => {
            // handle error
            console.log(erro);
        })
    }

    return (
        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
            <input type="hidden" id="anime_id" name="anime_id" ref={register} />
            <FormGroup>
                <label className="col-sm-2 control-label">Nome do anime</label>
                <div className="col-sm-4">
                    <input
                        name="anime_nome"
                        component="input"
                        type="text"
                        placeholder="Nome do Anime"
                        className="form-control"
                        onChange={generateLink}
                        ref={register({ required: true })}
                    />
                    {errors.anime_nome && 'Nome é um campo obrigatório'}
                </div>
                <label className="col-sm-1 control-label">TMDBID</label>
                <div className="col-sm-4">
                    <input
                        name="anime_imdb"
                        component="input"
                        type="text"
                        placeholder="Ex: 00000"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.anime_imdb && 'IMDB é um campo obrigatório'}
                </div>
                <Button bsStyle="info" fill onClick={() => { LoadData(watch("anime_imdb")) }} >
                    Carregar
                </Button>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Nome original</label>
                <div className="col-sm-4">
                    <input
                        name="anime_titulo_original"
                        component="input"
                        type="text"
                        placeholder="Ex: ワンピース"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.anime_titulo_original && 'Nome original é um campo obrigatório'}
                </div>
                <label className="col-sm-1 control-label">Temporada</label>
                <div className="col-sm-5">
                    <input
                        name="anime_temporadas"
                        component="input"
                        type="number"
                        placeholder="Ex: 19"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.anime_temporadas && 'Temporada é um campo obrigatório'}
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Sinopse</label>
                <div className="col-sm-10">
                    <textarea
                        name="anime_sinopse"
                        placeholder="Ex: Era uma vez..."
                        className="form-control"
                        rows="5" cols="33"
                        ref={register({ required: true })}
                    />
                    {errors.anime_sinopse && 'Sinopse é um campo obrigatório'}
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Ano do Anime</label>
                <div className="col-sm-4">
                    <input
                        name="anime_ano"
                        component="input"
                        type="text"
                        placeholder="1999"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.anime_ano && 'Ano do Anime é um campo obrigatório'}
                </div>
                <label className="col-sm-1 control-label">Situação</label>
                <div className="col-sm-5">
                    <select
                        name="anime_situacao"
                        component="select"
                        className="form-control"
                        ref={register({ required: true })}
                    >
                        <option value="">Selecione a Situação...</option>
                        {statusTv.map(statusTvOption => (
                            <option value={statusTvOption} key={statusTvOption}>
                                {statusTvOption}
                            </option>
                        ))}
                    </select>
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Imagem Capa</label>
                <div className="col-sm-4">
                    <input
                        name="anime_imagem"
                        component="input"
                        type="text"
                        placeholder="Ex: /3tkDMNfM2YuIAJlvGO6rfIzAnfG.jpg"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.anime_imagem && 'Imagem Capa é um campo obrigatório'}
                    <br></br>
                    <div className="img-container">
                        <img alt="..." src={`https://image.tmdb.org/t/p/w185/${watch("anime_imagem")}`} />
                    </div>
                </div>
                <label className="col-sm-1 control-label">Imagem BackGround</label>
                <div className="col-sm-5">
                    <input
                        name="anime_bg"
                        component="input"
                        type="text"
                        placeholder="Ex: /3tkDMNfM2YuIAJlvGO6rfIzAnfG.jpg"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.anime_bg && 'Imagem BackGround é um campo obrigatório'}
                    <br></br>
                    <div className="img-container">
                        <img alt="..." src={`https://image.tmdb.org/t/p/w342/${watch("anime_bg")}`} />
                    </div>
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Status</label>
                <div className="col-sm-10">
                    <div className="checkbox">
                        <input
                            name="anime_status"
                            id="anime_status"
                            component="input"
                            type="checkbox"
                            defaultChecked
                            ref={register}
                        />
                        <label htmlFor="anime_status">Ativo</label>
                    </div>
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Giga Anime ID</label>
                <div className="col-sm-10">
                    <input
                        name="gp"
                        component="input"
                        type="text"
                        placeholder="Ex: 1234"
                        className="form-control"
                        ref={register}
                    />
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">Categorias</label>
                <div className="col-sm-10">
                    {categories.map(cat => (
                        <div className="checkbox" style={{ float: 'left', marginTop: '0px' }} key={cat.categoria_id}>
                            <input
                                name="categoria"
                                id={cat.categoria_nome}
                                component="input"
                                type="checkbox"
                                value={parseInt(cat.categoria_id)}
                                ref={register}
                            />
                            <label htmlFor={cat.categoria_nome}>{cat.categoria_nome}</label>
                        </div>
                    ))}
                </div>
            </FormGroup>
            <FormGroup>
                <label className="col-sm-2 control-label">URL</label>
                <div className="col-sm-10">
                    <input
                        name="url"
                        component="input"
                        type="text"
                        placeholder="Ex: nome-do-artigo"
                        className="form-control"
                        ref={register({ required: true })}
                    />
                    {errors.url && 'url é um campo obrigatório'}
                </div>
            </FormGroup>
            <button type="submit" className="btn-fill btn btn-info" disabled={!formState.isValid}>Submit</button>
        </form>
    )
}

class EditarAnimes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: null,
            show: false,
            dados: {}
        };
        this.successEdit = this.successEdit.bind(this);
    }

    successEdit(titulo, texto) {
        this.setState({
            alert: (
                <SweetAlert
                    success
                    style={{ display: "block", marginTop: "-100px" }}
                    title={titulo}
                    onConfirm={() => this.hideAlert()}
                    onCancel={() => this.hideAlert()}
                    confirmBtnBsStyle="info"
                > {texto}
                </SweetAlert>
            )
        });
    }
    hideAlert() {
        this.setState({
            alert: null
        });
    }
    componentDidMount() {
        const { id } = this.props.match.params
        api.get(`/admin/anime/${id}`).then((response) => {
            if (response.status === 200)
                this.setState({ dados: response.data[0] });

        }).catch((erro) => {
            // handle error
            console.log(erro);
        })
    }
    render() {
        const { dados } = this.state
        return (
            <div className="main-content">
                {this.state.alert}
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <h4 className="title">Editar Animes</h4>
                            {dados.anime_nome !== undefined &&
                                <Card
                                    content={
                                        <FormEdit data={dados} successEdit={this.successEdit} />
                                    }
                                />}
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default EditarAnimes;
